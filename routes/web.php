<?php

Route::get('/', 'BaseController@index')->name('base');

Route::get('/orders', 'Orders\OrdersController@index')->name('orders');

Route::resource('/order', 'Orders\OrdersController');