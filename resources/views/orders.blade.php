@extends('layouts.app')

@section('content')
<div class="container">
  <table class="table table-striped">
  <thead>
      <tr>
        <th>ID</th>
        <th>Partners name</th>
        <th>Price</th>
        <th>Name products</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($orders as $order)
        <tr>
          <td><a href="{{route('order.edit', $order->id)}}" target="_blank">{!! $order->id !!}</a></td>
          <td>{!! $partners[$order->partner_id] !!}</td>
          <td>
            @forelse ($order->order_products as $order_product)
            <li>{!! $order_product->price !!}</li>
            @empty
              ---
            @endforelse
          </td>
          <td>
            @forelse ($order->products($order->id) as $product)
              <li>{{ $product->name }}</li>
            @empty
              ---
            @endforelse
          </td>
          <td>
            @if ($order->status == 0)
              Новый
            @elseif ($order->status == 10)
              Подтвержден
            @elseif ($order->status == 20)
              Завершен
            @else
              Статус не определен.
            @endif
          </td>
        </tr>
      @empty
        <tr>
          <td colspan="3" class="text-center"><h2>Данные отсутствуют</h2></td>
        </tr>
      @endforelse
    </tbody>
    <tfoot>
        <tr>
            <td colspan="3">
                <ul class="pagination pull-right">
                    {{$orders->links()}}
                </ul>
            </td>
        </tr>
    </tfoot>
  </table>
</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
@endsection
