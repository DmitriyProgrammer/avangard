@extends('layouts.app')

@section('content')
<div class="container">
    <div class="title m-b-md">
        Avangard
    </div>
    <ul class="list-group">
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Weather in London
            <span class="badge badge-primary badge-pill">
                {!! $wheather['temp'] !!} degrees Celsius
            </span>
        </li>
    </ul>
    <div class="list-group">
    <a href="{{route('orders')}}" class="list-group-item active">
        <span class="glyphicon glyphicon-star"></span> Заказы <span class="badge">10</span>
    </a>
    <a href="#" class="list-group-item">
        <span class="glyphicon glyphicon-user"></span> Пользователи <span class="badge">24</span>
    </a>
    <a href="#" class="list-group-item">
        <span class="glyphicon glyphicon-th-list"></span> Статьи <span class="badge">411</span>
    </a>
    <a href="#" class="list-group-item">
        <span class="glyphicon glyphicon-camera"></span> Изображение <span class="badge">25</span>
    </a>
</div>
</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
@endsection