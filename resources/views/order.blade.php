@extends('layouts.app')

@section('content')
<div class="container">
<form class="form-horizontal" action="{{route('order.update', $order)}}" method="post">
    <input type="hidden" name="_method" value="put">
    <input type="hidden" name="partner_id" value="{{$order->partner_id}}">
    {{ csrf_field() }}
    <table class="table table-striped">
        <thead>
            <tr>
            <th>email_клиента</th>
            <th>партнер</th>
            <th>статус заказа</th>
            <th>продукты</th>
            <th>стоимость заказа</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><input type="text" class="form-control" name="client_email" placeholder="Заголовок категории" value="{!! $order->client_email !!}"></td>
                <td>
                    @foreach ($order->partner as $partner)
                    <input type="text" class="form-control" name="name" placeholder="Заголовок категории" value="{{ $partner->name }}">
                    @endforeach
                </td>
                <td>
                    <select class="form-control" name="status">
                        @if ($order->status == 0)
                            <option value="0" selected="">Новый</option>
                            <option value="10">Подтвержден</option>
                            <option value="20">Завершен</option>
                        @elseif ($order->status == 10)
                            <option value="10" selected="">Подтвержден</option>
                            <option value="0">Новый</option>
                            <option value="20">Завершен</option>
                        @elseif ($order->status == 20)
                            <option value="20" selected="">Завершен</option>
                            <option value="0">Новый</option>
                            <option value="10">Подтвержден</option>
                        @else
                            <option value="">Статус не определен</option>
                        @endif
                    </select>
                </td>
                <td>
                    @forelse ($order->products($order->id) as $product)
                        <li>{{ $product->name }}</li>
                    @empty
                        Данные отсутствуют
                    @endforelse
                </td>
                <td>
                    @forelse ($order->products($order->id) as $product)
                        <li>{{ $product->price }}</li>
                    @empty
                        Данные отсутствуют
                    @endforelse
                </td>
            </tr>
        </tbody>
    </table>
    <input class="btn btn-primary" type="submit" value="Сохранить">
  </form>
</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
@endsection