<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class BaseController extends Controller
{
    public function index()
    {
        // TO DO // transfer to services
        $client = new Client();
        $res = $client->request('GET', 'https://samples.openweathermap.org/data/2.5/weather?q=London&appid=b6907d289e10d714a6e88b30761fae22');
        $pat = "!\":\{(.*)\},!sUi";
        $n = preg_match_all($pat, (string) $res->getBody(), $result);
        if ($n>=1) {
            $pat1 = "!\"(.*)\":(.*),!sUi";
            $n1 = preg_match_all($pat1, $result[1][1], $result1);
            if ($n1>=1) {
                $mainTemperature[$result1[1][0]] = $result1[2][0]-273;
            }
        } 

        return view('base', [
            'wheather' => $mainTemperature
        ]);
    }
}
