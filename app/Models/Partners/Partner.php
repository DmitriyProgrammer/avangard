<?php

namespace App\Models\Partners;

use Illuminate\Database\Eloquent\Model;
use App\Models\Orders\Order;

class Partner extends Model
{

    protected $fillable = ['id','email','name', 'created_at', 'updated_at'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

}