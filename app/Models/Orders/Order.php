<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;
use App\Models\Order_products\Order_products;
use App\Models\Partners\Partner;

class Order extends Model
{

    protected $fillable = ['id','status','client_email', 'partner_id', 
        'delivery_dt', 'created_at', 'updated_at'];

    public function order_products()
    {
      return $this->hasMany(Order_products::class, 'order_id', 'id');
    }

    public function partner()
    {
      return $this->hasMany(Partner::class, 'id', 'partner_id');
    }

    public function products($order_id)
    {
      return self::join('order_products','orders.id', '=', 'order_products.order_id')
        ->join('products', 'order_products.product_id', '=', 'products.id')
        ->where('orders.id', $order_id)
        ->get();
    }

} 