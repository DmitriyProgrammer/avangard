<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = ['id','name','price', 'vendor_id', 'created_at', 'updated_at'];

}
