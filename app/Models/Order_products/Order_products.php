<?php

namespace App\Models\Order_products;

use Illuminate\Database\Eloquent\Model;

class Order_products extends Model
{

    protected $fillable = ['id','order_id','product_id', 'quantity', 
        'price', 'created_at', 'updated_at'];

}
